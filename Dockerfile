FROM node:18-alpine

ARG GIT_HASH

RUN mkdir -p /opt
WORKDIR /opt

RUN echo $GIT_HASH

COPY node_modules /opt/node_modules
COPY dist /opt/dist
COPY .eslint* package.json yarn.lock /opt/

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn dist:run
