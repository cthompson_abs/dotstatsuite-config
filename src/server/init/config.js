import '../env'

export default async ctx => {
  const server = { host: process.env.SERVER_HOST || '0.0.0.0', port: Number(process.env.SERVER_PORT) || 80 };
  const config = {
    gitHash: process.env.GIT_HASH || 'local',
    env: process.env.NODE_ENV,
    server,
  }

  if(!['minio', 'gke', 'fs', 'azure', 'aws'].includes(process.env.BUCKET_PROVIDER)) throw new Error(`Wrong bucket provider: ${process.env.BUCKET_PROVIDER}!`)

  config.buckets = { 
    assets: process.env.ASSETS_BUCKET || 'assets', 
    configs: process.env.CONFIGS_BUCKET || 'configs', 
    i18n: process.env.I18N_BUCKET || 'i18n',
  }

  if(process.env.BUCKET_PROVIDER === 'fs'){
    config.bucketsProvider = {
      name: 'fs',
      dataDir: process.env.DATA_DIR,
    }
  } else if(process.env.BUCKET_PROVIDER === 'minio'){
    config.bucketsProvider = {
      name: 'minio',
      endpoint: process.env.MINIO_ENDPOINT,
      port: Number(process.env.MINIO_PORT),
      secretKey: process.env.MINIO_SECRET_KEY,
      accessKey: process.env.MINIO_ACCESS_KEY,
    }
  } else if (process.env.BUCKET_PROVIDER === 'gke') {
    config.bucketsProvider = {
      name: 'gke',
      keyFilename: process.env.GKE_KEY_FILE,
      projectId: process.env.GOOGLE_CLOUD_PROJECT,
    }
  } else if (process.env.BUCKET_PROVIDER === 'azure') {
    config.bucketsProvider = {
      name: 'azure',
      accountName: process.env.ACCOUNT_NAME,
      accountKey: process.env.ACCOUNT_KEY,
      sas: process.env.SAS,
      containerName: process.env.CONTAINER_NAME,
    }
  } else if (process.env.BUCKET_PROVIDER === 'aws') {
    config.bucketsProvider = {
      name: 'aws',
      region: process.env.REGION,
      proxy: process.env.USE_PROXY,
      bucketName: process.env.BUCKET_NAME,
      accessKeyId: process.env.ACCESS_KEY_ID,
      secretAccessKey: process.env.SECRET_ACCESS_KEY,
      sessionToken: process.env.SESSION_TOKEN_KEY,
    }
  }

  return ctx({ startTime: new Date(), config });
};
