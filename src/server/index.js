import 'core-js/stable';
import 'regenerator-runtime/runtime';
import { initResources } from 'jeto';
import debug from './debug'
import initConfig from './init/config';
import initHttp from './init/http';
import initRouter from './init/router';
import initBuckets from './init/buckets';

const resources = [
  initConfig, 
  ctx => {
    debug.info(ctx().config, 'running config');
    return ctx;
  },
  initBuckets,
  initRouter, 
  initHttp
];

initResources(resources)
  .then(() => debug.info('🚀 server started'))
  .catch(err => debug.error(err));
