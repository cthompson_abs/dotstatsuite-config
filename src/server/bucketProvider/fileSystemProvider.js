import path from 'path'
import { createReadStream } from 'fs'

const downloadFile = (dataDir, bucketName) => async id => {
  const filePath = path.join(dataDir, bucketName, id)
  return createReadStream(filePath)
};

export default config => {
  const dataDir = config.bucketsProvider.dataDir
  const res = bucketName => {
    return {
      name: 'fs',
      downloadFile: downloadFile(dataDir, bucketName),
    };
  };

  return res;
};
