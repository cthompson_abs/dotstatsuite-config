import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";
import { addProxyToClient } from 'aws-sdk-v3-proxy'

const downloadFile = (s3Client, bucketname, awsBucketName) => async id => {
  const filePath = bucketname + id;
  const command = new GetObjectCommand({
    Bucket: awsBucketName,
    Key: filePath
  });

  const item = await s3Client.send(command);
  return item.Body;
};

export default config => {
  let conf = {
    region: config.bucketsProvider.region,
    bucketName: config.bucketsProvider.bucketName,
  };

  // If the accessKeyId is not null, then add the credentails to the payload.
  // CLI will ignore blank values
  if (config.bucketsProvider.accessKeyId){
    conf.credentials = {
      accessKeyId: config.bucketsProvider.accessKeyId,
      secretAccessKey: config.bucketsProvider.secretAccessKey,
      sessionToken: config.bucketsProvider.sessionToken,
    }
  }

  // AWS CLI does not respect device proxy settings.
  // This will use the required package if proxy is require.
  const s3Client = config.bucketsProvider.proxy == "true"
    ? addProxyToClient(new S3Client(conf))
    : new S3Client(conf);

  const res = bucketname => {
    return {
      name: 'aws',
      downloadFile: downloadFile(s3Client, bucketname, config.bucketsProvider.bucketName),
    };
  };

  return res;
};