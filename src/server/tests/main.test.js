import got from 'got';
import { initResources } from 'jeto';
import { Readable } from 'stream';
import initHttp from '../init/http';
import initRouter from '../init/router';

const initBuckets = ctx => {
  const bucketProvider = name => ({
    name,
    downloadFile : id => Promise.resolve(Readable.from(Buffer.from(id))),
  });

  return ctx({ bucketProvider });
};

const initConfig = ctx => ctx({
  config: {
    buckets:{
      assets: 'assets',
      configs: 'configs',
      i18n: 'i18n',
    },
    isProduction: false,
    server: {
      host: 'localhost',
    },
  },
});

let CTX;

// beforeAll(() => initResources([ initConfig, initBuckets, initRouter, initHttp])
beforeAll(() => initResources([ initConfig, initBuckets, initRouter, initHttp]).then(ctx => { CTX = ctx }));

afterAll(() => { if(CTX) CTX().httpServer.close() });

describe('server', () => {
  it('should serve configs', async () => {
    const { headers, body } = await got.get(`${CTX().httpServer.url}/configs/test/data.json`) ;
    expect(body).toEqual('/test/data.json');
    expect(headers['content-type'].indexOf('application/json')).not.toEqual(-1);
  });

  it('should serve assets', async () => {
    const { headers, body } = await got.get(`${CTX().httpServer.url}/assets/test/toto/a.css`)
    expect(body).toEqual('/test/toto/a.css');
    expect(headers['content-type'].indexOf('text/css')).not.toEqual(-1);
  });

  it('should serve i18n', async () => {
    const { headers, body } = await got.get(`${CTX().httpServer.url}/i18n/test/i18n/fr.json`);
    expect(body).toEqual('/test/i18n/fr.json');
    expect(headers['content-type'].indexOf('application/json')).not.toEqual(-1);
  });

  it('should not serve', (done) => {
    got.get(`${CTX().httpServer.url}/other/test/i18n/fr.json`).catch(() => done());
  });
});
