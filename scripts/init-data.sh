#!/bin/sh 
DIR_CONFIGS='./configs'
DIR_ASSETS='./assets'
DIR_I18N='./i18n'
RED='\033[0;31m';
GREEN='\033[0;32m';
YELLOW='\033[0;33m';
NC='\033[0m';
if [ ! -d $DIR_CONFIGS ] || [ ! -d $DIR_ASSETS ] || [ ! -d $DIR_I18N ]; then
  git clone -b develop https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config-data.git gen-config-data
  if [ ! -d $DIR_CONFIGS ]; then
    rm -rf $DIR_CONFIGS
    mv gen-config-data/configs configs;
    if [ $? -eq 0 ]; then
      echo -e "$GREEN[OK] configs folder added $NC "
    else
      echo -e "$RED[X] configs folder failed to be import $NC"
    fi
  else
    echo -e "$YELLOW[!] configs folder already exist $NC"
  fi

  if [ ! -d $DIR_ASSETS ]; then
    rm -rf $DIR_ASSETS
    mv gen-config-data/assets assets;
    if [ $? -eq 0 ]; then
      echo -e "$GREEN[OK] assets folder added $NC "
    else
      echo -e "$RED[X] assets folder failed to be imported $NC"
    fi
  else
    echo -e "$YELLOW[!] assets folder already exist $NC"
  fi

  if [ ! -d $DIR_I18N ]; then
    rm -rf $DIR_I18N
    mv -v gen-config-data/i18n i18n;
    if [ $? -eq 0 ]; then
      echo -e "$GREEN[OK] i18n folder added $NC "
    else
      echo -e "$RED[X] i18n folder failed to be import $NC"
    fi
  else
    echo -e "$YELLOW[!] i18n folder already exist $NC"
  fi

  rm -rf gen-config-data
else
  echo -e "$YELLOW[!] It seems you already have those following folders $DIR_CONFIGS, $DIR_ASSETS, $DIR_I18N. $NC"
  echo "You can delete one or more to get the latest version"
fi